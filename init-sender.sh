# On the remote machine, source this in ~/.bashrc or some other file where it
# runs when you SSH in.

# Move the forwarded socket to a new temp folder, and set $SWISSH_SOCK.
# There are two reasons to do this:
# 1. If you SSH to the same server twice, it will fail to forward the socket
#    the second time unless something moves/removes it. We can't control this
#    from the client side: https://bugzilla.mindrot.org/show_bug.cgi?id=2601
# 2. In some cases, other users might be able to connect to a socket in /tmp,
#    if SSH doesn't make it private, or if the OS ignores permissions on sockets
#    (https://unix.stackexchange.com/a/105925/53198). Making a temp dir should
#    fix this - although there's a brief interval between creating & moving it.
#
# Experimentally, moving the socket only works within a filesystem.

if [ -S "/tmp/swissh-${USER}.sock" ]; then
    echo "Found Unix socket for swissh"
    SWISSH_SOCK="$(mktemp -d --tmpdir swissh.XXXXXX)/sock"
    export SWISSH_SOCK
    mv "/tmp/swissh-${USER}.sock" "$SWISSH_SOCK"
fi
