Send files back from an interactive SSH session.

This is a rough tool that works for me. Don't expect polish, configurability,
etc. There are three parts:

1. Receiver running on the local system (e.g. your laptop)::

      git clone https://gitlab.com/takluyver/swissh.git
      cd swissh

      pip install '.[receive]'
      cp swissh.service ~/.local/share/systemd/user/
      cp swissh.socket  ~/.local/share/systemd/user/
      systemctl --user --now enable swissh.socket

   The receiver is socket activated - systemd listens for connections, and
   launches the program. It shuts down once it's idle for 2 minutes.

2. SSH tunnel::

      ssh -R /tmp/swissh-%r.sock:$XDG_RUNTIME_DIR/swissh/receive.sock user@hostname

   Or add a RemoteForward line in your ``~/.ssh/config``. You can also add
   forwarding to an open SSH session by pressing Return, ~, C to access a
   little control prompt.

3. Send files from the remote server::

      git clone https://gitlab.com/takluyver/swissh.git
      cd swissh
      pip install '.[send]'

      # Edit ~/.bashrc or ~/.zshrc or similar, adding:
      source path/to/swissh/init-sender.sh

      python3 -m swissh.send foo.pdf

   Files will be opened by default, or use ``--download`` to send them to your
   downloads folder. You can alias these to shorter commands.

**Security implications**: Processes on either the local or the remote side
which can connect to the Unix sockets can send you a file to open.
Normally, this should only be processes you run, but for safety
you will be prompted when a file is received - if you didn't expect a file,
you probably shouldn't open it.
