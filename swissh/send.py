"""Send a file to ~/.ssh/swissh_target"""
from argparse import ArgumentParser
import os
from pathlib import Path
from shutil import make_archive
import socket
from tempfile import TemporaryDirectory

from tqdm import tqdm

from .utils import fmt_bytes

BLOCK_SIZE = 32 * 1024


def send(verb, path: Path, from_dir=False):
    if path.is_dir():
        with TemporaryDirectory() as td:
            basename = path.resolve().name
            zip_file = make_archive(os.path.join(td, basename), 'zip', path)
            return send(verb, Path(zip_file), True)

    elif not path.is_file():
        raise FileNotFoundError(path)

    sock = socket.socket(socket.AF_UNIX)
    sock.connect(os.environ['SWISSH_SOCK'])

    file_size = path.stat().st_size

    # Send header (\v = vertical tab, marking end of header)
    hdr = (f'{verb}:{path.name}\n'
           f'extract={from_dir}\n'
           f'size={file_size}\v')
    sock.sendall(hdr.encode('utf-8'))

    print(f"Sending {path.name} ({fmt_bytes(file_size)})...")

    with path.open('rb') as f:
        # tqdm gives us a progress bar as we read the file
        with tqdm.wrapattr(f, "read", total=file_size) as fobj:
            while True:
                data = fobj.read(BLOCK_SIZE)
                if not data:
                    break  # EOF
                sock.sendall(data)

    sock.close()


def main(argv=None):
    ap = ArgumentParser('swissh.send')
    ap.add_argument('--download', action='store_true')
    ap.add_argument('file', type=Path)
    args = ap.parse_args(argv)

    verb = 'SAVE' if args.download else 'OPEN'
    send(verb, args.file)

if __name__ == '__main__':
    main()
