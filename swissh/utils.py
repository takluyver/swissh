BYTES_UNITS = ['B', 'KiB', 'MiB', 'GiB', 'TiB']

def fmt_bytes(n: int) -> str:
    n = float(n)
    for suffix in BYTES_UNITS:
        if n < 1024:
            break
        n /= 1024

    return f'{n:.1f} {suffix}'
