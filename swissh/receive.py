"""Listen on $XDG_RUNTIME_DIR/swissh_local for files sent
"""

from dataclasses import dataclass
from enum import Enum
import logging
import math
import os
from pathlib import Path
from shutil import unpack_archive
from tempfile import mkdtemp

from jeepney.io.trio import open_dbus_router
import trio

from .notifications import NotificationSender

log = logging.getLogger(__name__)
IDLE_TIMEOUT = 120
PROMPT_RECV_SIZE = 100 * 1024 * 1024  # Prompt if file size > 100 MiB
MAX_HEADER_SIZE = 100 * 1024  # Reject if header > 100 KiB

class BadDataError(Exception):
    pass

class Verb(Enum):
    OPEN = 1
    SAVE = 2

@dataclass
class Request:
    verb: Verb
    filename: str
    size: int
    extract: bool

_downloads_dir_cache = None

async def get_downloads_dir() -> Path:
    global _downloads_dir_cache
    if _downloads_dir_cache is None:
        r = await trio.run_process(['xdg-user-dir', 'DOWNLOAD'], capture_stdout=True)
        _downloads_dir_cache = Path(r.stdout.decode('utf-8').strip())
    return _downloads_dir_cache


async def make_path(verb, filename: str) -> Path:
    if verb is Verb.SAVE:
        dl_dir = await get_downloads_dir()
        path = dl_dir / filename
        i = 0
        while path.exists():
            i += 1
            stem, ext = os.path.splitext(filename)
            path = dl_dir / f'{stem} ({i}){ext}'
            if i >= 10000:
                raise Exception("Can't make unused filename")
        return path
    else:  # OPEN
        return Path(mkdtemp(prefix='swissh_'), filename)


class FileHandler:
    def __init__(self, stream: trio.abc.Stream, activity_cb):
        self.stream = stream
        self._activity = activity_cb
        self._content_start = b''

    async def process_header(self) -> Request:
        pieces = []
        header_bytes_rcvd = 0
        async for data in self.stream:
            self._activity()
            pieces.append(data)
            header_bytes_rcvd += len(data)
            if header_bytes_rcvd >= MAX_HEADER_SIZE:
                raise BadDataError(f"> {MAX_HEADER_SIZE} bytes for header")
            if b'\v' in data:
                break
        else:
            raise BadDataError("Stream finished without complete header")

        header, self._content_start = b''.join(pieces).split(b'\v', 1)

        try:
            return self.parse_header(header)
        except Exception:
            raise BadDataError("Error parsing header")

    @staticmethod
    def parse_header(data: bytes):
        lines = data.decode('utf-8').split('\n')
        verb_s, filename = lines[0].split(':', 1)
        kv = dict(l.split('=', 1) for l in lines[1:])
        return Request(
            Verb[verb_s], filename, int(kv['size']), kv.get('extract') == 'True'
        )

    async def process_body(self, path: Path, exp_size: int):
        log.info(f"Writing %d bytes to %s", exp_size, path)
        bytes_written = 0
        with path.open('wb') as f:
            f.write(self._content_start)
            bytes_written += len(self._content_start)
            async for data in self.stream:
                self._activity()
                f.write(data)
                bytes_written += len(data)
                if bytes_written > exp_size:
                    log.warning("Got more than %d bytes content", exp_size)
                    break

        if bytes_written != exp_size:
            log.info("Deleting incompletely transferred file %s", path)
            path.unlink()
            raise BadDataError(f"Expected {exp_size} bytes, got {bytes_written}")


class App:
    def __init__(self, notifier, cancel_scope):
        self.notifier = notifier
        self.cancel_scope = cancel_scope

    def activity(self):
        self.cancel_scope.deadline = max(
            self.cancel_scope.deadline, trio.current_time() + IDLE_TIMEOUT
        )

    async def notify_recvd(self, verb, path):
        if verb is Verb.SAVE:
            await self.notifier.notify_downloaded(path)
        else:  # OPEN
            await self.notifier.notify_open(path)

    async def handle_connection(self, stream):
        try:
            self.activity()
            handler = FileHandler(stream, self.activity)
            req = await handler.process_header()

            if req.size > PROMPT_RECV_SIZE:
                if not (await self.notifier.prompt_big_file(req.filename, req.size)):
                    return  # Transfer rejected

            path = await make_path(req.verb, req.filename)
            await handler.process_body(path, req.size)
            if req.extract:
                dir_path = await make_path(req.verb, Path(req.filename).stem)
                unpack_archive(path, dir_path)
                os.unlink(path)
                path = dir_path
            await self.notify_recvd(req.verb, path)

        except BadDataError:
            log.warning("Received bad data", exc_info=True)
        finally:
            await stream.aclose()


async def listen(sock, idle_timeout):
    listener = trio.SocketListener(sock)
    async with open_dbus_router() as dbus_rtr:
        async with trio.open_nursery() as nursery:
            nursery.cancel_scope.deadline = trio.current_time() + idle_timeout
            notifier = NotificationSender(dbus_rtr)
            nursery.start_soon(notifier.listen_closed)
            nursery.start_soon(notifier.listen_actions)
            app = App(notifier, nursery.cancel_scope)

            await trio.serve_listeners(
                app.handle_connection, [listener], handler_nursery=nursery,
            )

        log.info("Exiting (idle)")


async def main():
    logging.basicConfig(level=logging.INFO)
    listen_fds = sd_listen_fds()
    if listen_fds:
        idle_timeout = IDLE_TIMEOUT
        log.info(
            "Got %d fds from systemd, listening on fd=%d until idle for %d seconds",
            len(listen_fds), listen_fds[0], idle_timeout
        )
        sock = trio.socket.socket(fileno=listen_fds[0])
        socket_path = None
    else:
        idle_timeout = math.inf
        socket_path = Path(os.environ['XDG_RUNTIME_DIR'], 'swissh', 'receive.sock')
        socket_path.parent.mkdir(parents=True, exist_ok=True)
        log.info("No file descriptors, binding %s", socket_path)
        sock = trio.socket.socket(trio.socket.AF_UNIX)
        await sock.bind(socket_path)
        sock.listen()

    try:
        await listen(sock, idle_timeout)
    finally:
        if socket_path is not None:
            log.info("Removing socket %s", socket_path)
            socket_path.unlink()


def sd_listen_fds():
    """Get FDs sent by systemd. Python-ish version of the C function"""
    if os.environ.get('LISTEN_PID', '') == str(os.getpid()):
        listen_fds = os.environ.get('LISTEN_FDS', '')
        try:
            n_fds = int(listen_fds)
        except ValueError:
            n_fds = 0
        fds = list(range(3, 3+n_fds))  # 3 = SD_LISTEN_FDS_START
        for fd in fds:
            os.set_inheritable(fd, False)
        return fds
    return []


if __name__ == '__main__':
    trio.run(main)
