"""Desktop notifications for received files
"""
import logging
from pathlib import Path

from jeepney.io.trio import open_dbus_router, Proxy
from jeepney import (
    MatchRule, MessageGenerator, MessageType, message_bus, new_method_call,
)
import trio

from .utils import fmt_bytes

log = logging.getLogger(__name__)

## Auto-generated classes from jeepney.bindgen ------------------------------
# Manual tweaking: parameter names, default values

class Notifications(MessageGenerator):
    interface = 'org.freedesktop.Notifications'

    def __init__(self, object_path='/org/freedesktop/Notifications',
                 bus_name='org.freedesktop.Notifications'):
        super().__init__(object_path=object_path, bus_name=bus_name)

    def Notify(self, app_name, replaces_id, app_icon, summary, body, actions=(), hints={}, expire_timeout=-1):
        return new_method_call(
            self, 'Notify', 'susssasa{sv}i',
            (app_name, replaces_id, app_icon, summary, body, list(actions), hints, expire_timeout)
        )

    def CloseNotification(self, id):
        return new_method_call(self, 'CloseNotification', 'u',
                               (id,))

    def GetCapabilities(self):
        return new_method_call(self, 'GetCapabilities')

    def GetServerInformation(self):
        return new_method_call(self, 'GetServerInformation')


class FileManager1(MessageGenerator):
    interface = 'org.freedesktop.FileManager1'

    def __init__(self, object_path='/org/freedesktop/FileManager1',
                 bus_name='org.freedesktop.FileManager1'):
        super().__init__(object_path=object_path, bus_name=bus_name)

    def ShowFolders(self, URIs, StartupId=''):
        return new_method_call(self, 'ShowFolders', 'ass',
                               (URIs, StartupId))

    def ShowItems(self, URIs, StartupId=''):
        return new_method_call(self, 'ShowItems', 'ass',
                               (URIs, StartupId))

    def ShowItemProperties(self, URIs, StartupId=''):
        return new_method_call(self, 'ShowItemProperties', 'ass',
                               (URIs, StartupId))

## End of auto-generated classes -------------------------------------

class BoolResponse:   # Event + value (not quite a Future: no error passing)
    def __init__(self):
        self._event = trio.Event()
        self._value = None

    def set(self, value=True):
        self._value = value
        self._event.set()

    async def wait(self):
        await self._event.wait()
        return self._value


class NotificationSender:
    def __init__(self, router):
        self.router = router
        self._notify_proxy = Proxy(Notifications(), router)
        self._paths = {}     # nid: path
        self._responses = {}  # nid: BoolResponse

    async def prompt_big_file(self, name, size):
        resp = BoolResponse()
        nid = await self.notify_big_file(name, size)
        self._responses[nid] = resp
        res = False
        try:
            with trio.move_on_after(60) as cscope:
                res = await resp.wait()
        finally:
            self._responses.pop(nid, None)
        log.debug("Accept transfer: %s (timeout: %s)", res, cscope.cancelled_caught)
        return res

    async def notify_big_file(self, name, size):
        nid, = await self._notify_proxy.Notify(
            'swissh', 0, '', 'Receive large file?', f'{name} ({fmt_bytes(size)})',
            actions=['accept', 'Accept', 'reject', 'Reject']
        )
        return nid

    async def notify_open(self, path: Path):
        """Send a notification prompting the user to open a file"""
        nid,  = await self._notify_proxy.Notify(
            'swissh', 0, '', 'Open file?', f'Received {path.name}',
            actions=['open', 'Open', 'delete', 'Delete']
        )
        self._paths[nid] = path

    async def notify_downloaded(self, path: Path):
        """Send a notification for a file saved to Downloads"""
        nid, = await self._notify_proxy.Notify(
            'swissh', 0, '', 'Downloaded file', f'Received {path.name}',
            actions=['open_dir', 'Show in folder']
        )
        self._paths[nid] = path

    async def listen_closed(self):
        """Clean up paths when notifications are closed"""
        mr = MatchRule(
            type=MessageType.signal,
            interface='org.freedesktop.Notifications',
            member='NotificationClosed'
        )
        await Proxy(message_bus, self.router).AddMatch(mr)
        with self.router.filter(mr) as receive_channel:
            async for msg in receive_channel:
                nid, reason = msg.body
                log.debug("Notification %r closed (reason code: %r)", nid, reason)
                self._paths.pop(nid, None)

    async def listen_actions(self):
        """Listen for actions invoked on notifications"""
        mr = MatchRule(
            type=MessageType.signal,
            interface='org.freedesktop.Notifications',
            member='ActionInvoked'
        )
        await Proxy(message_bus, self.router).AddMatch(mr)
        with self.router.filter(mr) as receive_channel:
            async for msg in receive_channel:
                nid, action_key = msg.body
                log.info("Action %r from notification ID %r",
                         action_key, nid)

                action_method = getattr(self, f'action_{action_key}', None)
                if action_method:
                    await action_method(nid)

    ## Handler methods for actions (buttons clicked on notifications):

    async def action_accept(self, nid):
        if resp := self._responses.get(nid, None):
            resp.set(True)

    async def action_reject(self, nid):
        if resp := self._responses.get(nid, None):
            resp.set(False)

    async def action_open(self, nid):
        if path := self._paths.get(nid, None):
            await trio.run_process(['xdg-open', str(path)])

    async def action_delete(self, nid):
        if path := self._paths.get(nid, None):
            path.unlink(missing_ok=True)

    async def action_open_dir(self, nid):
        if path := self._paths.get(nid, None):
            await Proxy(FileManager1(), self.router).ShowItems([
                f'file://{path.resolve()}'
            ])
